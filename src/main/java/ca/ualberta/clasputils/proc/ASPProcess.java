package ca.ualberta.clasputils.proc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract class ASPProcess implements Cloneable {
	private Process proc;

	private ArrayList<String> command = new ArrayList<String>();

	private Redirect output = Redirect.PIPE;

	ASPProcess(String programName) {
		command.add(programName);
	}

	public final void addOptions(String... options) {
		addOptions(Arrays.asList(options));
	}

	public final void addOptions(List<String> options) {
		this.command.addAll(options);
	}

	public final InputStream start() {
		try {
			addFinalOptions();
			ProcessBuilder pb = new ProcessBuilder(command);
			pb.redirectError(Redirect.INHERIT);
			if (output != null) {
				pb.redirectOutput(output);
			}
			proc = pb.start();
			final OutputStream os = proc.getOutputStream();
			new Thread() {
				@Override
				public void run() {
					try {
						sendInput(os);
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}.start();
			if (output.type() == Redirect.Type.PIPE)
				return proc.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	void sendInput(OutputStream out) throws IOException {
	}

	void addFinalOptions() {
	}

	public final boolean redirectOutput(Redirect r) {
		if (!r.equals(output)) {
			output = r;
			return true;
		} else
			return false;
	}

	public final void destroy() {
		proc.destroy();
	}

	public final int waitFor() throws InterruptedException {
		return proc.waitFor();
	}

	@Override
	protected ASPProcess clone() {
		ASPProcess rec;
		try {
			rec = (ASPProcess) super.clone();
			@SuppressWarnings("unchecked")
			ArrayList<String> cloneCommand = (ArrayList<String>) command.clone();
			rec.command = cloneCommand;
			return rec;
		} catch (CloneNotSupportedException e) {
			throw new Error("This should never happen");
		}
	}

	ASPProcess clone(String newCommand) {
		ASPProcess result = clone();
		result.command.set(0, newCommand);
		return result;
	}
}
