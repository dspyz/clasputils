package ca.ualberta.clasputils.proc;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.io.ByteStreams;

public final class GringoProcess extends ASPProcess {
	private static final String GRINGO_COMMAND = "gringo";

	private static class CRPair {
		final ClassLoader loader;
		final String resourceName;
		final boolean noExternal;

		CRPair(ClassLoader loader, String resourceName, boolean noExternal) {
			this.loader = loader;
			this.resourceName = resourceName;
			this.noExternal = noExternal;
		}
	}

	private final ArrayList<CRPair> resources = new ArrayList<CRPair>();
	private final ArrayList<String> lines = new ArrayList<String>();
	private List<String> procfile = Collections.emptyList();

	public GringoProcess() {
		this(GRINGO_COMMAND);
	}

	public GringoProcess(String gringoCommand) {
		super(gringoCommand);
	}

	public void addFile(ClassLoader loader, String f, boolean noExternal) {
		resources.add(new CRPair(loader, f, noExternal));
	}

	public void addFile(String f, boolean noExternal) {
		addFile(null, f, noExternal);
	}

	public void addFile(ClassLoader loader, String f) {
		addFile(loader, f, false);
	}

	public void addFile(String f) {
		addFile(null, f);
	}

	public void addLine(String l) {
		lines.add(l);
	}

	public void setProcFile(String procFileName) {
		procfile = Collections.singletonList(procFileName);
	}

	private void writeToStream(OutputStream out) throws IOException {
		PrintStream ps = new PrintStream(new BufferedOutputStream(out));
		for (CRPair r : resources) {
			final InputStream in;
			if (r.loader == null) {
				in = new FileInputStream(r.resourceName);
			} else {
				in = r.loader.getResourceAsStream(r.resourceName);
			}
			if (r.noExternal) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(in));
				for (;;) {
					String line = reader.readLine();
					if (line == null)
						break;
					if (line.trim().startsWith("#external"))
						continue;
					ps.println(line);
				}
			} else {
				ByteStreams.copy(in, ps);
				ps.println();
			}
		}
		for (String line : lines) {
			ps.println(line);
		}
		ps.close();
	}

	@Override
	void addFinalOptions() {
		if (!procfile.isEmpty()) {
			assert procfile.size() == 1;
			try {
				FileOutputStream fos = new FileOutputStream(procfile.get(0));
				writeToStream(fos);
				fos.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else if (lines.isEmpty()) {
			procfile = new ArrayList<String>();
			for (CRPair p : resources) {
				if (p.loader == null) {
					procfile.add(p.resourceName);
				} else {
					procfile.clear();
					break;
				}
			}
		}
		addOptions(procfile);
	}

	@Override
	void sendInput(OutputStream out) throws IOException {
		if (procfile.isEmpty())
			writeToStream(out);
	}
}
