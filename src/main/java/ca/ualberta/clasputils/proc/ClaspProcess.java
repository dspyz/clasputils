package ca.ualberta.clasputils.proc;

import java.io.IOException;
import java.io.OutputStream;

import ca.ualberta.clasputils.io.OutputStreamWrapper;

public final class ClaspProcess extends ASPProcess implements Cloneable {
	private final class ClaspOSWrapper extends OutputStreamWrapper {
		ClaspOSWrapper(OutputStream under) {
			super(under);
		}

		@Override
		public void close() throws IOException {
			super.close();
			finishedWriting();
		}
	}

	private static final String CLASP_COMMAND = "clasp";

	private String inputFile = null;
	private ClaspOSWrapper out;
	private boolean writing = false;

	public ClaspProcess() {
		this(CLASP_COMMAND);
	}

	public ClaspProcess(String claspCommand) {
		super(claspCommand);
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public synchronized OutputStream getOutputStream()
			throws InterruptedException {
		while (!writing)
			wait();
		return out;
	}

	private synchronized void finishedWriting() {
		writing = false;
		notifyAll();
	}

	@Override
	synchronized void sendInput(OutputStream out) throws IOException {
		if (inputFile == null) {
			this.out = new ClaspOSWrapper(out);
			writing = true;
			notifyAll();
			try {
				while (writing)
					wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	void addFinalOptions() {
		if (inputFile != null)
			addOptions(inputFile);
	}

	public ClaspProcess clone(String newCommand) {
		return (ClaspProcess) super.clone(newCommand);
	}

	public boolean hasInputFile() {
		return inputFile != null;
	}
}
