package ca.ualberta.clasputils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;

import ca.ualberta.clasputils.io.WaiterInputStream;
import ca.ualberta.clasputils.parse.SolutionReader;
import ca.ualberta.clasputils.proc.ClaspProcess;
import ca.ualberta.clasputils.proc.GringoProcess;

import com.google.common.io.ByteStreams;

public class Utils {
	public static SolutionReader runGringoClasp(GringoProcess proc1,
			String... claspOptions) {
		return runGringoClasp(null, proc1, claspOptions);
	}

	public static SolutionReader runGringoClasp(String groundFile,
			final GringoProcess proc1, String... claspOptions) {
		boolean lastOnly = false;
		ArrayList<String> newOptions = new ArrayList<String>(
				claspOptions.length);
		for (String s : claspOptions) {
			if (s.equalsIgnoreCase("lastOnly"))
				lastOnly = true;
			else
				newOptions.add(s);
		}
		if (groundFile == null)
			proc1.redirectOutput(Redirect.PIPE);
		else
			proc1.redirectOutput(Redirect.to(new File(groundFile)));

		final InputStream grounded = proc1.start();

		final ClaspProcess cp = new ClaspProcess();
		cp.addOptions(newOptions);
		InputStream res;
		if (groundFile == null) {
			res = cp.start();
			new Thread() {
				public void run() {
					try {
						OutputStream out = cp.getOutputStream();
						ByteStreams.copy(grounded, out);
						out.close();
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
				}
			}.start();
		} else {
			assert grounded == null;
			cp.setInputFile(groundFile);
			final WaiterInputStream wis = new WaiterInputStream();
			new Thread() {
				@Override
				public void run() {
					try {
						proc1.waitFor();
						wis.setUnder(cp.start());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}.start();
			res = wis;
		}
		return new SolutionReader(res, lastOnly);
	}
}
