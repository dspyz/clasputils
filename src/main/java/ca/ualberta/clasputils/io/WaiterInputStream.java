package ca.ualberta.clasputils.io;

import java.io.IOException;
import java.io.InputStream;

public class WaiterInputStream extends InputStreamWrapper {
	private boolean closed = false;
	private int readLimit = -1;

	public WaiterInputStream() {
		super(null);
	}

	@Override
	public int read() throws IOException {
		waitForUnder();
		return super.read();
	}

	@Override
	public int read(byte b[]) throws IOException {
		waitForUnder();
		return super.read(b);
	}

	@Override
	public int read(byte b[], int off, int len) throws IOException {
		waitForUnder();
		return super.read(b, off, len);
	}

	@Override
	public long skip(long n) throws IOException {
		waitForUnder();
		return super.skip(n);
	}

	private synchronized void waitForUnder() {
		while (underIsNull())
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}

	@Override
	public synchronized void close() throws IOException {
		if (underIsNull())
			closed = true;
		else
			super.close();
	}

	@Override
	public synchronized void mark(int readlimit) {
		if (underIsNull())
			this.readLimit = readlimit;
		else
			super.mark(readlimit);
	}

	@Override
	public synchronized void reset() throws IOException {
		if (!underIsNull())
			super.reset();
	}

	@Override
	public boolean markSupported() {
		if (underIsNull())
			return false;
		// There is no way to know yet, so we'll be safe
		else
			return super.markSupported();
	}

	@Override
	public synchronized void setUnder(InputStream in) {
		if (!underIsNull())
			throw new UnsupportedOperationException("Already set");
		try {
			super.setUnder(in);
			if (closed)
				super.close();
			else if (readLimit >= 0)
				super.mark(readLimit);
			notifyAll();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}