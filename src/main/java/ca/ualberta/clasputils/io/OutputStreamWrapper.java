package ca.ualberta.clasputils.io;

import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamWrapper extends OutputStream {
	private final OutputStream under;

	public OutputStreamWrapper(OutputStream under) {
		this.under = under;
	}

	@Override
	public void write(int b) throws IOException {
		under.write(b);
	}

	@Override
	public void write(byte b[]) throws IOException {
		under.write(b);
	}

	@Override
	public void write(byte b[], int off, int len) throws IOException {
		under.write(b, off, len);
	}

	public void flush() throws IOException {
		under.flush();
	}

	public void close() throws IOException {
		under.close();
	}
}
