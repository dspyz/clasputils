package ca.ualberta.clasputils.io;

import java.util.ArrayDeque;
import java.util.Scanner;

public final class PutbackScanner {
	private final Scanner scan;
	private final ArrayDeque<String> tokens = new ArrayDeque<String>();

	public PutbackScanner(Scanner scan) {
		this.scan = scan;
	}

	public void putBack(String s) {
		tokens.push(s);
	}

	public String next() {
		String res = tokens.poll();
		if (res == null)
			return scan.next();
		else
			return res;
	}

	public boolean hasNext() {
		return !tokens.isEmpty() || scan.hasNext();
	}
}
