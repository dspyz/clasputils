package ca.ualberta.clasputils.io;

import java.io.IOException;
import java.io.InputStream;

public class InputStreamWrapper extends InputStream {
	private InputStream under;

	public InputStreamWrapper(InputStream under) {
		this.under = under;
	}

	@Override
	public int read() throws IOException {
		return under.read();
	}

	@Override
	public int read(byte b[]) throws IOException {
		return under.read(b);
	}

	@Override
	public int read(byte b[], int off, int len) throws IOException {
		return under.read(b, off, len);
	}

	@Override
	public long skip(long n) throws IOException {
		return under.skip(n);
	}

	@Override
	public void close() throws IOException {
		under.close();
	}

	@Override
	public synchronized void mark(int readlimit) {
		under.mark(readlimit);
	}

	@Override
	public synchronized void reset() throws IOException {
		under.reset();
	}

	@Override
	public boolean markSupported() {
		return under.markSupported();
	}

	protected void setUnder(InputStream in) {
		under = in;
	}

	protected final boolean underIsNull() {
		return under == null;
	}
}
