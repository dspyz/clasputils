package ca.ualberta.clasputils;

import java.util.List;

import ca.ualberta.clasputils.parse.SolutionEvent;
import ca.ualberta.clasputils.parse.SolutionListener;
import ca.ualberta.clasputils.parse.SolutionReader;
import ca.ualberta.clasputils.proc.ClaspProcess;

public class Approximator implements SolutionListener {
	private final ClaspProcess procMax;
	private final ClaspProcess procMin;
	private final int margin;
	private SolutionReader readerMax;
	private SolutionReader readerMin;
	private String bestSolution = null;
	private int bestMin, bestMax;
	private boolean finished = false;

	public Approximator(ClaspProcess procMax, List<String> pMaxOpts,
			List<String> pMinOpts, String optheu, int margin) {
		if (!procMax.hasInputFile())
			throw new RuntimeException(
					"proc must have input file for approximation");
		this.procMax = procMax;
		procMin = procMax.clone("unclasp");
		procMax.addOptions(pMaxOpts);
		procMin.addOptions(pMinOpts);
		procMin.addOptions("--opt-uncore=" + optheu);
		this.margin = margin;
	}

	public synchronized void start() {
		readerMax = new SolutionReader(procMax.start(), true);
		readerMax.addSolutionListener(this);
		readerMin = new SolutionReader(procMin.start(), true);
		readerMin.addSolutionListener(this);
		new Thread() {
			@Override
			public void run() {
				try {
					procMax.waitFor();
					done();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	@Override
	public synchronized void eventOccurred(SolutionEvent e) {
		if (finished)
			return;
		if (e.source == readerMax) {
			if (e.isMax()) {
				bestMax = e.getMax();
			} else if (e.isSolution()) {
				bestSolution = e.getSolution();
			}
		} else if (e.source == readerMin) {
			if (e.isMin())
				bestMin = e.getMin();
		}
		if (bestMax - bestMin <= margin) {
			done();
		}
	}

	public synchronized void done() {
		procMax.destroy();
		procMin.destroy();
		finished = true;
		notifyAll();
	}

	public synchronized String getSolution() throws InterruptedException {
		while (!finished)
			wait();
		return bestSolution;
	}
}
