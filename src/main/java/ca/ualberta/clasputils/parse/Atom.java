package ca.ualberta.clasputils.parse;

import java.util.List;

public final class Atom {
	public final String name;
	public final Predicate pred;
	public final ASPObject[] args;

	public Atom(Predicate pred, List<ASPObject> args) {
		this.pred = pred;
		this.args = args.toArray(new ASPObject[0]);
		this.name = pred.apply(args);
	}

	@Override
	public String toString() {
		return name;
	}

	public String getLine() {
		return name + ".";
	}
}
