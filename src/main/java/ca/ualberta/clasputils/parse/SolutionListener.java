package ca.ualberta.clasputils.parse;

public interface SolutionListener {
	public void eventOccurred(SolutionEvent e);
}
