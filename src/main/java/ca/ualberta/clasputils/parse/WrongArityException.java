package ca.ualberta.clasputils.parse;
public final class WrongArityException extends RuntimeException {
	private static final long serialVersionUID = -1975481431802767461L;

	public WrongArityException(String message) {
		super(message);
	}
}
