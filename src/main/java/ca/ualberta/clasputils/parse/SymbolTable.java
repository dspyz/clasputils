package ca.ualberta.clasputils.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import ca.ualberta.clasputils.io.PutbackScanner;

public final class SymbolTable {
	private static SymbolTable defaultTable;

	public static SymbolTable getTable() {
		if (defaultTable == null)
			defaultTable = new SymbolTable();
		return defaultTable;
	}

	private final HashMap<String, ASPFunction> functions = new HashMap<String, ASPFunction>();
	private final HashMap<String, ASPObject> objects = new HashMap<String, ASPObject>();
	private final HashMap<String, Predicate> predicates = new HashMap<String, Predicate>();
	private final HashMap<String, Atom> atoms = new HashMap<String, Atom>();

	private final HashMap<Integer, ASPObject> integers = new HashMap<Integer, ASPObject>();

	public ASPFunction getFun(String name, int arity) {
		return getFun(name, arity, functions, funFact);
	}

	public Predicate getPred(String name, int arity) {
		return getFun(name, arity, predicates, predFact);
	}

	private <T> T getFun(String name, int arity,
			HashMap<String, T> functionsTable, Factory<T> fact) {
		String fullname = combine(name, arity);
		T res = functionsTable.get(fullname);
		if (res == null) {
			res = fact.newInstance(name, arity);
			functionsTable.put(fullname, res);
		}
		return res;
	}

	static String combine(String name, int arity) {
		name = name.trim();
		String fullName = name + "/" + arity;
		return fullName;
	}

	public ASPObject getObj(String s) {
		return getObj(makeScanner(s));
	}

	private String replaceDelimeters(String s) {
		return s.replaceAll("\\(", " ( ").replaceAll("\\,", " , ")
				.replaceAll("\\)", " ) ");
	}

	private ASPObject getObj(PutbackScanner argsScanner) {
		String objName = argsScanner.next();
		try {
			int val = Integer.parseInt(objName);
			return getInt(val);
		} catch (NumberFormatException nfe) {
		}
		List<ASPObject> objList = getEnclosedObjList(argsScanner);
		if (objList.isEmpty())
			return getLit(objName);
		else
			return getObj(getFun(objName, objList.size()), objList);
	}

	public Atom getAtom(String s) {
		return getAtom(makeScanner(s));
	}

	private PutbackScanner makeScanner(String s) {
		return new PutbackScanner(new Scanner(replaceDelimeters(s)));
	}

	private Atom getAtom(PutbackScanner argsScanner) {
		String predName = argsScanner.next();
		final List<ASPObject> objList = getEnclosedObjList(argsScanner);
		return getAtom(getPred(predName, objList.size()), objList);
	}

	private List<ASPObject> getEnclosedObjList(PutbackScanner argsScanner) {
		final List<ASPObject> objList;
		if (!argsScanner.hasNext())
			objList = Collections.emptyList();
		else {
			String oparen = argsScanner.next();
			if (!oparen.equals("(")) {
				argsScanner.putBack(oparen);
				objList = Collections.emptyList();
			} else {
				objList = getCSLObjList(argsScanner);
				String closeParen = argsScanner.next();
				if (!closeParen.equals(")")) {
					throw new ASPFormatException("No close-paren for "
							+ objList);
				}
			}
		}
		return objList;
	}

	public List<Atom> getAtoms(String line) {
		PutbackScanner scan = makeScanner(line);
		ArrayList<Atom> atoms = new ArrayList<Atom>();
		while (scan.hasNext())
			atoms.add(getAtom(scan));
		return atoms;
	}

	private Atom getAtom(Predicate pred, List<ASPObject> objList) {
		String name = pred.apply(objList);
		Atom at = atoms.get(name);
		if (at == null) {
			at = new Atom(pred, objList);
			atoms.put(name, at);
		}
		return at;
	}

	private ASPObject getObj(ASPFunction fun, List<ASPObject> objList) {
		String name = fun.apply(objList);
		ASPObject res = objects.get(name);
		if (res == null) {
			res = new ASPObject(fun, objList);
			objects.put(name, res);
		}
		return res;
	}

	private List<ASPObject> getCSLObjList(PutbackScanner argsScanner) {
		ArrayList<ASPObject> res = new ArrayList<ASPObject>();
		for (;;) {
			res.add(getObj(argsScanner));
			if (!argsScanner.hasNext())
				break;
			String sep = argsScanner.next();
			if (!sep.equals(",")) {
				argsScanner.putBack(sep);
				break;
			}
		}
		return res;
	}

	private ASPObject getLit(String objName) {
		ASPObject res = objects.get(objName);
		if (res == null) {
			res = new ASPObject(objName);
			objects.put(objName, res);
		}
		return res;
	}

	public ASPObject getInt(int val) {
		ASPObject res = integers.get(val);
		if (res == null) {
			res = new ASPObject(val);
			integers.put(val, res);
		}
		return res;
	}

	private static interface Factory<T> {
		public T newInstance(String name, int arity);
	}

	private static Factory<ASPFunction> funFact = new Factory<ASPFunction>() {
		@Override
		public ASPFunction newInstance(String name, int arity) {
			return new ASPFunction(name, arity);
		}
	};

	private static Factory<Predicate> predFact = new Factory<Predicate>() {
		@Override
		public Predicate newInstance(String name, int arity) {
			return new Predicate(name, arity);
		}
	};
}
