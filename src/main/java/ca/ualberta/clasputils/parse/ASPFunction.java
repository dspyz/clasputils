package ca.ualberta.clasputils.parse;

import java.util.List;

public final class ASPFunction {
	public final String name;
	public final int arity;

	public ASPFunction(String name, int arity) {
		this.name = name.trim();
		this.arity = arity;
	}

	@Override
	public String toString() {
		return SymbolTable.combine(name, arity);
	}

	public String apply(List<ASPObject> args) {
		return apply(name, arity, args);
	}

	public static String apply(String name, int arity, List<ASPObject> args) {
		if (arity != args.size())
			throw new WrongArityException("Expected list of length " + arity
					+ " but got " + args.size());
		StringBuilder nameBuilder = new StringBuilder();
		nameBuilder.append(name);
		boolean first = true;
		for (ASPObject arg : args) {
			if (first) {
				nameBuilder.append('(');
				first = false;
			} else
				nameBuilder.append(',');
			nameBuilder.append(arg);
		}
		if (!first)
			nameBuilder.append(')');
		return nameBuilder.toString();
	}
}
