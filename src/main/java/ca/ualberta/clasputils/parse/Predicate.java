package ca.ualberta.clasputils.parse;

import java.util.List;

public final class Predicate {
	private final String name;
	private final int arity;

	public Predicate(String name, int arity) {
		this.name = name;
		this.arity = arity;
	}

	public String apply(List<ASPObject> args) {
		return ASPFunction.apply(name, arity, args);
	}

	@Override
	public String toString() {
		return SymbolTable.combine(name, arity);
	}
}
