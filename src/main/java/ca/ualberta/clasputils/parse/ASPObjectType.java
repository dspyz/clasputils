package ca.ualberta.clasputils.parse;
public enum ASPObjectType {
	FUNCTION, LITERAL, INTEGER;
}
