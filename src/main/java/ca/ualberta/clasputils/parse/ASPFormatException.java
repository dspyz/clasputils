package ca.ualberta.clasputils.parse;
public final class ASPFormatException extends IllegalArgumentException {
	private static final long serialVersionUID = -5697366816351158635L;

	public ASPFormatException(String message) {
		super(message);
	}
}
