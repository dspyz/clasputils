package ca.ualberta.clasputils.parse;

public class SolutionEvent {
	public final Object source;
	private final String solution;
	private final int newMin, newMax;

	private SolutionEvent(Object source, String solution, int newMin, int newMax) {
		this.source = source;
		this.solution = solution;
		this.newMin = newMin;
		this.newMax = newMax;
	}

	SolutionEvent(Object source, String solution) {
		this(source, solution, -1, -1);
	}

	SolutionEvent(Object source, int newMin, int newMax) {
		this(source, null, newMin, newMax);
	}

	public boolean isMax() {
		return newMax >= 0;
	}

	public int getMax() {
		if (isMax())
			return newMax;
		else
			throw new UnsupportedOperationException();
	}

	public String getSolution() {
		if (isSolution())
			return solution;
		else
			throw new UnsupportedOperationException();
	}

	public boolean isSolution() {
		return solution != null;
	}

	public int getMin() {
		if (isMin())
			return newMin;
		else
			throw new UnsupportedOperationException();
	}

	public boolean isMin() {
		return newMin >= 0;
	}
}
