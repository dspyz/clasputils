package ca.ualberta.clasputils.parse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class SolutionReader {
	private static final class SolutionReportRunnable implements Runnable {
		private final SolutionListener l;
		private final SolutionEvent e;

		SolutionReportRunnable(SolutionListener l, SolutionEvent e) {
			this.l = l;
			this.e = e;
		}

		@Override
		public void run() {
			l.eventOccurred(e);
		}

	}

	private final class SolutionReaderThread extends Thread {
		@Override
		public void run() {
			boolean addNext = false;
			try {
				for (;;) {
					String line = reader.readLine();
					if (line == null)
						break;
					else if (addNext) {
						addNext = false;
						if (lastOnly)
							solutions = Collections.singletonList(line);
						else
							addSolutionLine(line);
						solOccurred();
					} else {
						if (line.startsWith("Answer:"))
							addNext = true;
						else if (line.startsWith("Optimization: ")) {
							bestOptimization = Integer.parseInt(line
									.substring(14));
							maxOccurred();
						} else if (line.startsWith("min: ")) {
							bestMin = Integer.parseInt(line.substring(5));
							// For use with unclasp
							minOccurred();
						}
						System.out.println(line);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			setFinished();
		}
	}

	private Executor exec = Executors.newCachedThreadPool();
	private final BufferedReader reader;
	private boolean finished = false;
	private volatile List<String> solutions;
	private volatile int bestOptimization = Integer.MAX_VALUE;
	private volatile int bestMin = 0;
	private final boolean lastOnly;
	private final LinkedHashSet<SolutionListener> listeners = new LinkedHashSet<SolutionListener>();

	public SolutionReader(InputStream in) {
		this(in, false);
	}

	public SolutionReader(InputStream in, boolean lastOnly) {
		reader = new BufferedReader(new InputStreamReader(in));
		this.lastOnly = lastOnly;
		if (!lastOnly)
			solutions = new ArrayList<String>();
		new SolutionReaderThread().start();
	}

	private synchronized void addSolutionLine(String line) {
		solutions.add(line);
	}

	private synchronized void minOccurred() {
		notifyListeners(new SolutionEvent(this, bestMin, -1));
	}

	private synchronized void maxOccurred() {
		notifyListeners(new SolutionEvent(this, -1, bestOptimization));
	}

	private synchronized void solOccurred() {
		notifyListeners(new SolutionEvent(this,
				solutions.get(solutions.size() - 1)));
	}

	private synchronized void notifyListeners(SolutionEvent e) {
		for (SolutionListener l : listeners) {
			exec.execute(new SolutionReportRunnable(l, e));
		}
	}

	private synchronized void setFinished() {
		finished = true;
		notifyAll();
	}

	public synchronized List<String> getSolutions() throws InterruptedException {
		while (!finished)
			wait();
		return solutions;
	}

	public synchronized String curSolution() {
		return solutions.get(solutions.size() - 1);
	}

	public int curMax() {
		return bestOptimization;
	}

	public int curMin() {
		return bestMin;
	}

	public synchronized boolean addSolutionListener(SolutionListener listener) {
		return listeners.add(listener);
	}

	public synchronized boolean removeSolutionListener(SolutionListener listener) {
		return listeners.remove(listener);
	}
}
