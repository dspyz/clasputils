package ca.ualberta.clasputils.parse;

import java.util.List;

public final class ASPObject {
	public final ASPObjectType type;

	public final String name;
	private final ASPFunction function;
	private final int value;
	private final ASPObject[] args;

	public ASPObject(String name) {
		type = ASPObjectType.LITERAL;
		this.name = name;
		function = null;
		value = 0;
		args = null;
	}

	public ASPObject(int val) {
		type = ASPObjectType.INTEGER;
		name = Integer.toString(val);
		function = null;
		value = val;
		args = null;
	}

	public ASPObject(ASPFunction fun, List<ASPObject> args) {
		if (args.size() != fun.arity)
			throw new WrongArityException(args.size()
					+ " arguments supplied to function " + fun);
		type = ASPObjectType.FUNCTION;

		name = fun.apply(args);

		function = fun;
		this.args = args.toArray(new ASPObject[0]);
		this.value = 0;
	}

	public ASPFunction getFunction() {
		if (type == ASPObjectType.FUNCTION)
			return function;
		else
			throw new UnsupportedOperationException("Object of type" + type
					+ " must be of type FUNCTION");
	}

	public int getValue() {
		if (type == ASPObjectType.INTEGER)
			return value;
		else
			throw new UnsupportedOperationException("Object of type " + type
					+ "must be of type INTEGER");
	}

	public ASPObject getArg(int i) {
		if (type == ASPObjectType.FUNCTION)
			return args[i];
		else
			throw new UnsupportedOperationException("Object of type" + type
					+ " must be of type FUNCTION");
	}

	@Override
	public String toString() {
		return name;
	}
}
