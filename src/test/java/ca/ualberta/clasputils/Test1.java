package ca.ualberta.clasputils;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import ca.ualberta.clasputils.parse.SolutionReader;
import ca.ualberta.clasputils.proc.ClaspProcess;
import ca.ualberta.clasputils.proc.GringoProcess;

public class Test1 {
	@Test
	public void test1() throws IOException, InterruptedException {
		GringoProcess proc1 = new GringoProcess();
		proc1.addFile(getClass().getClassLoader(), "test1.lp");

		SolutionReader reader = Utils.runGringoClasp(proc1);
		testCorrectness(reader);
	}

	@Test
	public void test2() throws IOException, InterruptedException {
		GringoProcess proc1 = new GringoProcess();
		proc1.addFile(getClass().getClassLoader(), "test1.lp");

		String tmpFile = Files.createTempFile("testGround", ".clasp")
				.toString();
		SolutionReader reader = Utils.runGringoClasp(tmpFile, proc1);
		testCorrectness(reader);
	}

	public void testCorrectness(SolutionReader reader)
			throws InterruptedException {
		List<String> sols = reader.getSolutions();

		Assert.assertEquals(1, sols.size());
		String sol = sols.get(0);
		String[] atoms = sol.split(" ");
		Arrays.sort(atoms);
		String[] expected = "a(1) a(2) a(3) a(4) a(5) b(5) b(4) b(3) b(2) b(1)"
				.split(" ");
		Arrays.sort(expected);
		Assert.assertArrayEquals(expected, atoms);
	}
}
